<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Order;

Route::get('/', function () {
    return "hi";
});

//Route::get('/api/orders', 'TaskController@getOrders');
Route::get('/api/orders/{id}', 'TaskController@getOrdersById');
Route::get('/api/customers', 'TaskController@getCustomers');

Route::get('/api/startUp', 'TaskController@startUp');
Route::post('/ordercompleted', 'WebhookController@index');
Route::get('/api/emaillastorder', 'TaskController@emailLastOrder');
Route::post('/api/sendemailstofriends', 'TaskController@emailFriends');
Route::get('/api/gifttotal', 'TaskController@giftTotal')->name('updatetotal');
Route::get('/api/sponsormatch/{id}', 'TaskController@sponsorMatch');
Route::get('/api/sponsormatch/', 'TaskController@allSponsorMatch');
Route::post('/api/addNameToDonorWall', 'TaskController@donorWall');
Route::post('/api/updateDonorName', 'TaskController@donorName');
Route::post('/api/saveDonorNote', 'TaskController@donorNote');
Route::get('/api/donorNames', 'TaskController@donorNames');
Route::get('/api/donorNotes', 'TaskController@donorNotes');
Route::get('/api/givingWall', 'TaskController@givingWall');
Route::get('/api/spon', 'TaskController@spon');
Route::get('/api/friendemails', 'TaskController@friendEmailList');

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|


Route::get('/', function () {
    return view('welcome');
});
*/
