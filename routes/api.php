<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;



Route::get('/gifts', 'GiftController@getGifts');
Route::get('/gifts/{id}', 'GiftController@getGift');

Route::get('/sponsors', 'SponsorController@getSponsors');
Route::get('/sponsors/{id}', 'SponsorController@getSponsor');

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
*/