<?php

namespace App;
use App\Gift;
use Illuminate\Database\Eloquent\Model;

class Sponsor extends Model
{
    public function sponsorMatch(){
        if ( $this->category != 'matching' ) {
            return null;
        } else {
        $orderTotal = Gift::where('sponsor_id', '=', $this->id)->first()->total;
            if (($orderTotal / $this->match_amount) < 1 ){
                return ($orderTotal / $this->match_amount) * 100;
            } else {
                return 100;
            }
        }
    }
    public function giftTotal(){
        if ( $this->category != 'matching' ) {
            return null;
        } else {
        $orderTotal = Gift::where('sponsor_id', '=', $this->id)->first()->total;
        return $orderTotal;
        }
    }
}
