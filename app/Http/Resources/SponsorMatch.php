<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SponsorMatch extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'gift_id' => $this->gift_id,
            'match_amount' => $this->match_amount,
            'match_progress' => $this->sponsorMatch(),
            'gift_total' => $this->giftTotal(),
            'img' => $this->img,
            'category' => $this->category,
            'website' => $this->website,
            'challenge_name' => $this->challenge_name,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
