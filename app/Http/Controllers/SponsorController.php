<?php

namespace App\Http\Controllers;
use App\Http\Resources\Sponsor as SponsorResource;
use App\Sponsor;
use Illuminate\Http\Request;

class SponsorController extends Controller
{
    public function getSponsors() {
        return new SponsorResource(Sponsor::all());
    }

    public function getSponsor($id) {
        return new SponsorResource(Sponsor::find($id));
    }
}
