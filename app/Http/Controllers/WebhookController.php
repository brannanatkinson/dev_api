<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Mail\DonationThankYou;
use App\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class WebhookController extends Controller
{
    public function index(Request $request){
        
        $order = Order::firstOrCreate([ 
            'billing_name' => $request->content['billingAddress']['fullName'], 
            'email' => $request->content['email'],
            'invoice' => $request->content['invoiceNumber'],
            'token' => $request->content['token']
        ]);
        //dump('order created');
        Mail::to($order->email)->send(new DonationThankYou($order));
        //return redirect()->action('TaskController@giftTotal');
        return redirect()->route('updatetotal');
    }
}
