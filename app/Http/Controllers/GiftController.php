<?php

namespace App\Http\Controllers;
use App\Http\Resources\Gift as GiftResource;
use App\Gift;

use Illuminate\Http\Request;

class GiftController extends Controller
{
    public function getGifts() {
        return new GiftResource(Gift::all());
    }

    public function getGift($id) {
        //return 'yep';
        return new GiftResource(Gift::find($id));
    }
}
