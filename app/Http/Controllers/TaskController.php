<?php

namespace App\Http\Controllers;
use App\Mail\DonationThankYou;
use App\Mail\ChallengeEmail;
use App\Order;
use App\Gift;
use App\Sponsor;
use App\FriendEmail;
use Illuminate\Http\Request;
use App\Http\Resources\SponsorMatch as SponsorMatchResource;
use Illuminate\Support\Facades\Mail;
use Mtownsend\SnipcartApi\SnipcartApi;

class TaskController extends Controller
{

    public function startUp() {
        $gifts = Gift::orderBy('value', 'ASC')->get();
        $sponsors = SponsorMatchResource::collection ( Sponsor::all() );
        $merged = collect(['gifts' => $gifts, 'sponsors' => $sponsors]);
        return ( $merged );
    }

    public function getOrdersById( $id ){
        return Order::where('token', '=', $id)->first();
    }

    public function emailFriends(Request $request) {
        $emails = $request->emails;
        $orderToken = $request->orderToken;
        $order = Order::where('token', '=', $orderToken)->first();
        foreach( $emails as $email ){
            Mail::to($email['emailAddress'])->send(new ChallengeEmail($order));
            FriendEmail::firstOrCreate([
                'email' => $email['emailAddress']
            ]);
        }
        $order->friendEmail = true;
        $order->friendEmailsSent += count($emails);
        $order->save();
    }

    public function giftTotal(){
        $apiKey = env('SNIPCART_KEY');
        $gifts = (new SnipcartApi($apiKey))->get()->from('/products')->send();
        $collection = collect($gifts)->recursive();
        $newGifts = $collection->all()['items'];
        $updatedGifts = $newGifts->each(function($item, $key){
            $giftId = $item['userDefinedId'];
            $giftToUpdate = Gift::where('giftShopId', '=', $giftId)->first();
            $giftToUpdate->total = $item['statistics']['totalSales'];
            $giftToUpdate->save();
            dump( $giftToUpdate );
        });
    }

    public function sponsorMatch( $id ) {
        $sponsor = Sponsor::find( $id );
        return $sponsor->id;
    }

    public function allSponsorMatch(){
        return SponsorMatchResource::collection ( Sponsor::all() );
    }

    public function donorWall(Request $request){
        $order = Order::where('token', '=', $request->orderToken)->first();
        $order->donorWall = $request->donorWallUpdate;
        $order->save();
    }

    public function donorName(Request $request){
        $order = Order::where('token', '=', $request->orderToken)->first();
        $order->billing_name = $request->billing_name;
        $order->save();
    }
    /*
    ** Returns donors to list on Giving Wall
    */
    public function donorNames(){
        $donors = Order::where('donorWall', '=', true)->orderBy('billing_name', 'ASC')->get();
        return $donors->toJson();
    }

    public function donorNote(Request $request){
        $order = Order::where('token', '=', $request->orderToken)->first();
        $order->note = $request->note;
        $order->showNote = $request->showNote;
        $order->save();
    }

    public function donorNotes(){
        $notes = Order::where('showNote', '=', true)->get();
        return $notes->toJson();
    }

    public function givingWall(){
        /*
        ** query Snipact API for products and add numberOfSales to get total gifts given
        */
        $apiKey = env('SNIPCART_KEY');
        $gifts = (new SnipcartApi($apiKey))->get()->from('/products')->send();
        $collection = collect($gifts)->recursive();
        $newGifts = $collection->all()['items'];
        $numberOfGifts = 0;
        foreach ($newGifts as $gift){
            $numberOfGifts += $gift['statistics']['numberOfSales'];
        }
        $totalDonations = 0;
        foreach ($newGifts as $gift){
            $totalDonations += $gift['statistics']['totalSales'];
        }
        /*
        ** query Snipact API for customers for total donors
        */
        $donors = (new SnipcartApi($apiKey))->get()->from('/customers')->send();
        $donorCollection = collect($donors)->recursive();
        $numberOfDonors = $donorCollection['totalItems'];
        

        return compact('numberOfGifts', 'numberOfDonors', 'totalDonations');
    }
    
    public function spon(){
        $gifts = Gift::orderBy('value', 'ASC')->get();
        $sponsors = SponsorMatchResource::collection ( Sponsor::all() );
        $merged = collect(['gifts' => $gifts, 'sponsors' => $sponsors]);
        return ( $merged );
    }

    public function friendEmailList(){
        $emails = FriendEmail::all();
        return ( $emails );
    }
}
