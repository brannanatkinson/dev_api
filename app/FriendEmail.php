<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FriendEmail extends Model
{
   protected $fillable = ['email'];
   protected $table = 'friendEmails';
}
