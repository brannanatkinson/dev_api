<?php

namespace App\Mail;
use App\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ChallengeEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $order;
    
    public function __construct(Order $order)
    {
        $this->order = $order;
        $this->mysub = $order->billing_name . ' thinks you will like Housing Hope';
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        
        return $this->from(['address' => 'office@maryparrish.org', 'name' => 'The Mary Parrish Center'])->subject($this->mysub)->view('emails.challenge');
    }
}
